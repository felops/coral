import * as React from 'react'
import { StatusBar } from 'expo-status-bar'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
} from '@apollo/client'

import { RootContext } from './src/context/Root'
import Navigation from './src/navigation'

const client = new ApolloClient({
  uri: 'https://api.spacex.land/graphql',
  cache: new InMemoryCache(),
})

export default function App() {
  const [favImages, setFavImages] = React.useState([] as string[])

  const toggleFavoriteImage = (image: string) => {
    if (favImages.includes(image)) {
      setFavImages(favImages.filter(img => img !== image))
      return
    }

    setFavImages([
      ...favImages,
      image,
    ])
  }

  return (
    <ApolloProvider client={client}>
      <RootContext.Provider value={{
        favoriteImages: favImages,
        toggleFavoriteImage,
      }}>
      <StatusBar style="auto" />
      <Navigation />
    </RootContext.Provider>
    </ApolloProvider>
  )
}
