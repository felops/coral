import { format } from 'date-fns'
import { Image, Pressable, StyleSheet, Text, View } from 'react-native'

const noImage = require('./../../../../../assets/no-image-placeholder.png')

interface ItemProps {
  missionName: string
  launchDateLocal: string
  flickerImages: string[]
  onPress: () => void
}

export default function Item({
  missionName,
  launchDateLocal,
  flickerImages,
  onPress,
}: ItemProps) {
  const image = flickerImages.length === 0
    ? noImage
    : { uri: flickerImages[flickerImages.length - 1] }

  return (
    <Pressable
      onPress={onPress}
      style={styles.pressable}
    >
      <Image
        source={image}
        style={styles.image}
      />
      <View>
        <Text style={styles.title}>{missionName}</Text>
        <Text style={styles.date}>
          {format(new Date(launchDateLocal), 'MM-dd-yyyy hh:mm a')}
        </Text>
      </View>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  pressable: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 5,
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 8,
  },
  image: {
    marginRight: 15,
    borderRadius: 8,
    overflow: 'hidden',
    width: 50,
    height: 50,
  },
  title: {
    fontWeight: 'bold',
  },
  date: {
    color: '#888',
  },
})
