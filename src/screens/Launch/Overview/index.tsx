import { ScrollView } from 'react-native'
import { useQuery } from '@apollo/client'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import Container from '../../../components/Container'
import Error from '../../../components/Error'
import Loading from '../../../components/Loading'
import LaunchesPast from '../../../contants/graphql/launchesPast'
import { LaunchStackParamList } from '../../../navigation'

import Item from './components/Item'

/* eslint-disable camelcase */
export interface QueryResult {
  mission_name: string
  launch_date_local: string
  links: {
    article_link: string,
    flickr_images: string[],
  },
  rocket: {
    rocket_name: string,
  },
}
/* eslint-enable camelcase */

export default function LaunchOverviewScreen({
  navigation,
}: NativeStackScreenProps<LaunchStackParamList, 'LaunchOverview'>) {
  const { loading: isLoading, error: hasError, data } = useQuery(LaunchesPast)

  const goToDetails = (item: QueryResult) => {
    navigation.navigate('LaunchDetails', { item })
  }

  const renderContent = () => {
    if (isLoading) {
      return <Loading />
    }

    if (hasError) {
      return <Error />
    }

    return (
      <ScrollView>
      {
        data.launchesPast.map((item: QueryResult) => (
          <Item
            flickerImages={item.links.flickr_images}
            key={item.mission_name}
            launchDateLocal={item.launch_date_local}
            missionName={item.mission_name}
            onPress={() => goToDetails(item)}
          />
        ))
      }
      </ScrollView>
    )
  }

  return (
    <Container>
      {renderContent()}
    </Container>
  )
}
