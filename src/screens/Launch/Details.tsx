import * as React from 'react'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import {
  Dimensions,
  Image,
  Text,
  Linking,
  Pressable,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native'
import { AntDesign } from '@expo/vector-icons'

import { LaunchStackParamList } from '../../navigation'
import Container from './../../components/Container'
import { RootContext } from '../../context/Root'

const { width } = Dimensions.get('window')

export default function LaunchDetailScreen({
  route,
}: NativeStackScreenProps<LaunchStackParamList, 'LaunchDetails'>) {
  const { favoriteImages, toggleFavoriteImage } = React.useContext(RootContext)
  const {
    links: {
      article_link: articleLink,
      flickr_images: flickerImages,
    },
    rocket: {
      rocket_name: rocketName,
    },
  } = route.params.item
  const imagesArray = flickerImages.length > 0
    ? flickerImages.slice(0, 3)
    : []

  const openLink = (link: string) => {
    Linking.openURL(link)
  }

  return (
    <Container>
      <ScrollView>
        <View style={styles.infoSection}>
          <Text style={styles.label}>ROCKET NAME</Text>
          <Text style={styles.rocketName}>{rocketName}</Text>
          {
            articleLink && (
              <>
                <Text style={styles.label}>ARTICLE LINK</Text>
                <Pressable onPress={() => openLink(articleLink)}>
                  <Text style={styles.link}>{articleLink}</Text>
                </Pressable>
              </>
            )
          }
        </View>
        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
          {
            imagesArray.map((image: string) => (
              <View key={image}>
                <Image
                  source={{ uri: image }}
                  style={styles.image}
                />
                <Pressable
                  onPress={() => toggleFavoriteImage(image)}
                  style={styles.favoriteBar}
                >
                {
                  favoriteImages.includes(image)
                    ? <>
                        <AntDesign color="yellow" name="star" size={28} />
                        <Text style={styles.favoriteText}>
                          Tap to remove from favorites
                        </Text>
                      </>
                    : <>
                      <AntDesign color="white" name="staro" size={28} />
                      <Text style={styles.favoriteText}>
                        Tap to add to favorites
                      </Text>
                    </>
                }
                </Pressable>
              </View>
            ))
          }
        </View>
      </ScrollView>
    </Container>
  )
}

const styles = StyleSheet.create({
  infoSection: {
    margin: 20,
  },
  label: {
    color: '#555',
    fontWeight: '700',
    textTransform: 'uppercase',
  },
  rocketName: {
    fontSize: 20,
    marginBottom: 15,
  },
  link: {
    color: 'blue',
  },
  image: {
    height: width,
    width: width,
  },
  favoriteBar: {
    padding: 10,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    flexDirection: 'row',
    alignItems: 'center',
  },
  favoriteText: {
    color: '#ccc',
    marginLeft: 6,
  },
})
