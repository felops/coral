import { StyleSheet, Text, View } from 'react-native'

export default function Error() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Ops, we were not able{'\n'}to load the launches
      </Text>
      <Text>
        Try again later
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
    lineHeight: 25,
    textAlign: 'center',
    marginBottom: 30,
  },
})
