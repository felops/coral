import { StyleSheet, View } from 'react-native'
import Colors from '../contants/Colors'

interface ContainerProps {
  children: JSX.Element
}

export default function Container({ children }: ContainerProps) {
  return (
    <View style={styles.container}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
})
