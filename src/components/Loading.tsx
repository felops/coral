import { ActivityIndicator, StyleSheet, View } from 'react-native'
import Colors from '../contants/Colors'

export default function Loading() {
  return (
    <View style={styles.container}>
      <ActivityIndicator color={Colors.primaryColor} size="large" />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
