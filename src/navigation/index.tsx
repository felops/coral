import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import Details from '../screens/Launch/Details'
import Overview, { QueryResult } from '../screens/Launch/Overview'

/* eslint-disable camelcase */
export type LaunchStackParamList = {
  LaunchOverview: undefined
  LaunchDetails: {
    item: QueryResult
  }
};
/* eslint-enable camelcase */

export const LaunchStack = createNativeStackNavigator<LaunchStackParamList>()

export default function Navigation() {
  return (
    <NavigationContainer>
      <LaunchStack.Navigator initialRouteName="LaunchOverview">
        <LaunchStack.Screen
          component={Overview}
          name="LaunchOverview"
          options={{ title: 'Latest Launches' }}
        />
        <LaunchStack.Screen
          component={Details}
          name="LaunchDetails"
          options={{ title: 'Launch Details' }}
        />
      </LaunchStack.Navigator>
    </NavigationContainer>
  )
}
