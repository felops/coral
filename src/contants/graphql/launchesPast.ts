import { gql } from '@apollo/client'

export default gql`
query GetPastLaunches {
  launchesPast(limit: 10, sort: "launch_date_local", order: "DESC") {
    mission_name
    launch_date_local
    launch_site {
      site_name_long
    }
    links {
      article_link
      flickr_images
    }
    rocket {
      rocket_name
    }
  }
}`
