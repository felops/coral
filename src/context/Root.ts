import { createContext } from 'react'

type ContextProps = {
  favoriteImages: string[]
  toggleFavoriteImage: (image: string) => void
}

export const RootContext = createContext<ContextProps>({
  favoriteImages: [],
  toggleFavoriteImage: () => {},
})
